
// Initiate RecommendText Objects from RecommendText.js for taste words
const increaseYield = new RecommendText(document.querySelectorAll(".increase-yield"), "Increase Yield", "<strong>Increase Yield: </strong>See the <a href='https://baristahustle.com/blog/espresso-recipes-measuring-yield/' target='_blank'>Barista Hustle post on espresso yield for more information.</a>");

const decreaseYield = new RecommendText(document.querySelectorAll(".decrease-yield"), "Decrease Yield", "<strong>Decrease Yield: </strong>See the <a href='https://baristahustle.com/blog/espresso-recipes-measuring-yield/' target='_blank'>Barista Hustle post on espresso yield for more information.</a>");

const improveExtraction = new RecommendText(document.querySelectorAll(".improve-extraction"), "Improve Extraction", "<strong>Improve Extraction: </strong>See the <a href='https://baristahustle.com/blog/coffee-extraction-and-how-to-taste-it/' target='_blank'>Barista Hustle post on espresso extraction for more information.</a>");

const decreaseYieldImproveExtraction = new RecommendText(document.querySelectorAll(".decrease-yield-improve-extraction"), "Decrease Yield and Improve Extraction", decreaseYield.text + "<br>" + improveExtraction.text);

const increaseYieldImproveExtraction = new RecommendText(document.querySelectorAll(".increase-yield-improve-extraction"), "Increase Yield and Improve Extraction", increaseYield.text + "<br>" + improveExtraction.text);

const good = new RecommendText(document.querySelectorAll(".good"), "Enjoy Your Espresso!", "You've hit the sweet spot, enjoy! Feel free to play around with yield to adjust the flavour.");


// Array of objects containing the problem type, header and recommendation text
const recObject = [increaseYield, decreaseYield, improveExtraction, decreaseYieldImproveExtraction, increaseYieldImproveExtraction, good];

// Iterate through the array, calling the clicky function from RecommendText.js on all objects
recObject.forEach(sendAlong => clicky(sendAlong.selection, sendAlong.header, sendAlong.text));