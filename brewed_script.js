
// Initiate RecommendText Objects from RecommendText.js for taste words
const emLc = new RecommendText(document.querySelectorAll(".em-lc"), "Extract More and Less Coffee", "<strong>Extract More: </strong>Finer grind and or longer brew time.<br \><strong>Less Coffee: </strong>A higher brew ratio can be achieved by fixing water weight and reducing dose OR by fixing dose and increasing water weight.");

const lc = new RecommendText(document.querySelectorAll(".lc"), "Less Coffee", "<strong>Less Coffee: </strong>A higher brew ratio which can be achieved by fixing water weight and reducing dose OR by fixing dose and increasing water weight.");

const elMC = new RecommendText(document.querySelectorAll(".el-mc"), "Extract Less and More Coffee", "<strong>Extract Less: </strong>Courser grind and/or shorter brew time.<br><br><strong>More Coffee: </strong>A lower brew ratio which can be achieved by fixing water weight and increasing dose OR by fixing dose and reducing water weight");

const el = new RecommendText(document.querySelectorAll(".el"), "Extract Less", "<strong>Extract Less: </strong>Courser grind and/or shorter brew time.");

const mc = new RecommendText(document.querySelectorAll(".mc"), "More Coffee", "<strong>More Coffee: </strong>A lower brew ratio which can be achieved by fixing water weight and increasing dose OR by fixing dose and reducing water weight");

const em = new RecommendText(document.querySelectorAll(".em"), "Extract More", "<strong>Extract More: </strong>Finer grind and or longer brew time.");

// Initiate RecommendText Objects from RecommendText.js for image
const centreImage = new RecommendText(document.querySelectorAll(".centreImage"), "Good Flavours", "You have selected the positive flavours in the middle, enjoy your brew!");

// Array of objects containing the problem type, header and recommendation text
const recObject = [emLc, lc, elMC, el, mc, em, centreImage];

// Iterate through the array, calling the clicky function from RecommendText.js on all objects
recObject.forEach(slingObjects => clicky(slingObjects.selection, slingObjects.header, slingObjects.text));