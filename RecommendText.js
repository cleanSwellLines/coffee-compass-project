class RecommendText {
    constructor(selection, header, text) {
        this.selection = selection;
        this.header = header;
        this.text = text;
    }

}

// To add and respond to click on taste words
function clicky(problemType, header, message) {
    for (i=0; i<problemType.length; i++) {
        //enable click event on each taste word and image
        problemType[i].addEventListener('click', () => {

            let x = event.target.textContent.toLowerCase();


            if (event.target.classList.contains('goodFlavour')) {
                // On click add recommendation text and button
                document.getElementById("recommendText").innerHTML =
                    `<div id='content' class='fixCoffeeMessage'>
                <h2>${header}</h2>
                <p>${message}</p> 
                <button id='resetButton' class='button'>&times;</button>
                </div>`;
            } else {
                // On click add recommendation text and button
                document.getElementById("recommendText").innerHTML =
                    `<div id='content' class='fixCoffeeMessage'><h3>You have selected <i>${x}</i> flavours:</h3>
                <h2>${header}</h2>
                <p>${message}</p> 
                <button id='resetButton' class='button'>&times;</button>
                </div>`;
            }


            /* Element to scroll to*/
            console.log(document.querySelector(".fixCoffeeMessage"));
            var elmnt = document.querySelector(".fixCoffeeMessage");
            elmnt.scrollIntoView();

            /*Call function to reset page*/
            var resetButton = document.getElementById("resetButton");
            resetPage(resetButton);
        })
    }
}

function resetPage(resetButton) {
    resetButton.addEventListener('click', function(){
        document.getElementById("recommendText").innerHTML = "";
        let topOfCompass = document.querySelector(".main-container");
        topOfCompass.scrollIntoView()
    })
}